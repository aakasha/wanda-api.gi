'use strict';

const AWS = require('aws-sdk'); // eslint-disable-line import/no-extraneous-dependencies
const validateOrder = require('../../model/order').validateOrder;

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.update = (event, context, callback) => {

  const timestamp = new Date().toISOString();
  const data = JSON.parse(event.body);
  const validate = validateOrder(data);
  
  if (!validate.status) {
    console.error('Validation Failed');
    console.error(validate.errors);
    callback(null, {
      statusCode: 400,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(validate.errors),
    });
    return;
  }

  const { name, phone, email, fromAddress, 
    toAddress, service, movingDate, note, 
    orderstatus, badgeclass
  } = data;

  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Key: {
      id: event.pathParameters.id,
    },
    ExpressionAttributeNames: {
      '#order_name': 'name',
    },
    ExpressionAttributeValues: {
      ':name': name,
      ':phone': phone,
      ':email': email,
      ':fromAddress': fromAddress,
      ':toAddress': toAddress,
      ':service': service,
      ':movingDate': movingDate,
      ':note': note,
      ':orderstatus': orderstatus,
      ':badgeclass': badgeclass,
      ':updatedAt': timestamp,
    },
    UpdateExpression: `SET #order_name = :name, phone = :phone, email = :email,
    fromAddress = :fromAddress, toAddress = :toAddress, service = :service, 
    movingDate = :movingDate, note = :note, orderstatus = :orderstatus,
    badgeclass = :badgeclass, updatedAt = :updatedAt`,
    ReturnValues: 'ALL_NEW',
  };

  // console.log(params);

  // update the order in the database
  dynamoDb.update(params, (error, result) => {
    // handle potential errors
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*'
      },
        body: 'Couldn\'t fetch the order item.',
      });
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      headers: { 'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*'
      },
      body: JSON.stringify(result.Attributes),
    };
    callback(null, response);
  });
};
