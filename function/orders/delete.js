'use strict';

const AWS = require('aws-sdk'); // eslint-disable-line import/no-extraneous-dependencies

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.delete = (event, context, callback) => {

  // const params = {
  //   TableName: process.env.DYNAMODB_TABLE,
  //   KeyConditionExpression: "#id = :uuid",
  //   ExpressionAttributeNames:{
  //       "#id": "id",
  //   },
  //   ExpressionAttributeValues: {
  //       ":uuid": event.pathParameters.id,
  //   }
  // };

  var params = {
    TableName:process.env.DYNAMODB_TABLE,
    Key:{
        "id": event.pathParameters.id,
    }
};

  console.log("Attempting a conditional delete...");
  console.log(params);

  dynamoDb.delete(params, function(err, data) {
      if (err) {
          console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
          callback(null, {
            statusCode: err.statusCode || 501,
            headers: { 'Content-Type': 'text/plain',
              'Access-Control-Allow-Origin': '*',
            },
            body: 'Couldn\'t remove the Order item.',
          });
      } else {
          console.log("DeleteItem succeeded:", JSON.stringify(data, null, 2));
          const response = {
            statusCode: 200,
            headers: { 'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify({}),
          };
          callback(null, response);
      }
  });
};
