'use strict';

const AWS = require('aws-sdk'); // eslint-disable-line import/no-extraneous-dependencies

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.get = (event, context, callback) => {

  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    KeyConditionExpression: "#id = :uuid",
    ExpressionAttributeNames:{
        "#id": "id",
    },
    ExpressionAttributeValues: {
        ":uuid": event.pathParameters.id,
    }
  };

  dynamoDb.query(params, function(err, data) {
    if (err) {
      console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'text/plain',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'GET,POST,PUT,DELETE,OPTIONS',
          'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Z-Key'
      },
        body: 'Couldn\'t fetch the order item.',
      });
    } else {
      console.log("Query succeeded.");
      const response = {
        statusCode: 200,
        headers: { 'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'GET,POST,PUT,DELETE,OPTIONS',
          'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Z-Key'
      },
        body: JSON.stringify(data.Items),
      };
      callback(null, response);
    }
});
};
