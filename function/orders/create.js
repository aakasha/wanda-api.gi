'use strict';

const shortid = require('shortid');
const AWS = require('aws-sdk');
const middy = require('@middy/core')
const cors = require('@middy/http-cors')
const validateOrder = require('../../model/order').validateOrder;

const dynamoDb = new AWS.DynamoDB.DocumentClient();

const create = async event => {

  const timestamp = new Date().toISOString();
  const orderdate = timestamp.substring(0,10);
  const data = JSON.parse(event.body);
  const validate = validateOrder(data);
  
  if (!validate.status) {
    console.error('Validation Failed');
    console.error(validate.errors);
    return {
      statusCode: 400,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(validate.errors),
    }
  }

  const { name, phone, email, fromAddress, 
          toAddress, service, movingDate, note, 
          orderstatus, badgeclass
        } = data;

  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Item: {
      id: shortid.generate(),
      orderdate,
      name,
      phone,
      email,
      fromAddress,
      toAddress,
      service,
      movingDate,
      note,
      orderstatus,
      badgeclass,
      createdAt: timestamp,
      updatedAt: timestamp,
    },
  };
  console.log("Query outside.");
  console.log(params);
  // write the order to the database

  try {
    const results = await dynamoDb.put(params).promise();
    console.log("Query succeeded.");
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(params.Item),
    };
  } catch (err) {
    console.error(err);
    return {
      statusCode: error.statusCode || 501,
      headers: { 
        'Content-Type': 'text/plain'
      },
      body: 'Couldn\'t create the order item.',
    };
  }
}

const handler = middy(create)
  .use(cors())

module.exports = { handler }