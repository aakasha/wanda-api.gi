# Serverless REST API

## Setup

```bash
npm i serverless -g
npm i
```

## Deploy

In order to deploy the endpoint simply run

```bash
serverless deploy
```

The expected result should be similar to:

```bash
Serverless: Packaging service…
Serverless: Uploading CloudFormation file to S3…
Serverless: Uploading service .zip file to S3…
Serverless: Updating Stack…
Serverless: Checking Stack update progress…
Serverless: Stack update finished…

Service Information
service: order-api-v2
stage: dev
region: eu-north-1
stack: order-api-v2-dev
resources: 40
api keys:
  None
endpoints:
  POST - https://zjtl4umfkg.execute-api.eu-north-1.amazonaws.com/dev/api/v2/orders
  GET - https://zjtl4umfkg.execute-api.eu-north-1.amazonaws.com/dev/api/v1/orders
  GET - https://zjtl4umfkg.execute-api.eu-north-1.amazonaws.com/dev/api/v1/orders/{id}
  PUT - https://zjtl4umfkg.execute-api.eu-north-1.amazonaws.com/dev/api/v1/orders/{id}
  DELETE - https://zjtl4umfkg.execute-api.eu-north-1.amazonaws.com/dev/api/v1/orders/{id}
functions:
  create: order-api-v2-dev-create
  list: order-api-v2-dev-list
  get: order-api-v2-dev-get
  update: order-api-v2-dev-update
  delete: order-api-v2-dev-delete
```
