const Ajv = require("ajv");
const addFormats = require("ajv-formats")
const ajv = new Ajv()
require("ajv-keywords")(ajv)
addFormats(ajv)

const schema = {
    type: "object",
    properties: {
        id: {type: "string"},
        orderdate: {type: "string", format: "date"},
        name: {type: "string", maxLength: 128},
        phone: {type: "string"},
        email: {type: "string", format: "email"},
        fromAddress: {type: "string"},
        toAddress: {type: "string"},
        orderstatus: {type: "string"},
        badgeclass: {type: "string"},
        service: {
            type: "array",
            items: {
                type: "string",
                transform: ["trim", "toLowerCase"],
                enum: ["wrap","pack","pickup"],
            }
        },
        movingDate: {
            type: "string",
            format: "date",
            formatMinimum: "2021-02-06",
            formatExclusiveMaximum: "2022-12-27",
        },
        note: {type: "string"},
        createdAt: {type: "string", format: "date"},
        updatedAt: {type: "string", format: "date"},
    },
    required: ["name", "phone", "email", "fromAddress", "toAddress", "service", "movingDate"],
    additionalProperties: false
}

const validate = ajv.compile(schema)

const data = {
    "name": "Rahul",
    "phone": "94480798",
    "email": "rahulapoorva1@gmail.com",
    "fromAddress": "19 Cantonment close",
    "toAddress": "6 Kitchener Link",
    "service": ["wrap","pack","pickup"],
    "movingDate": "2021-09-20",
    "note": ""
  }

module.exports.validateOrder = obj => {
    const valid = validate(obj)
   if (!valid) return {status: false, errors: validate.errors}
   else return {status: true}
}

